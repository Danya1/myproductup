package com.example.myproductsapp.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproductsapp.R;
import com.example.myproductsapp.models.DataStorage;
import com.example.myproductsapp.models.DbManager;
import com.example.myproductsapp.models.entities.Product;
import com.example.myproductsapp.views.main_view_tools.RvAdapter;
import com.example.myproductsapp.views.MainActivity;

import java.util.ArrayList;

public class ControllerMainActivity {
    private MainActivity mainActivity;
    private DbManager db;

    private ArrayList<Product> products;
    private RecyclerView rv;

    public ControllerMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

        LayoutInflater inflater = (LayoutInflater) mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = DbManager.getInstance(this.mainActivity.getApplicationContext());


    }

//    public void UpdateListViewProducts() {
//        ProductAdapter adapter = new ProductAdapter(products, mainActivity.getApplicationContext());
//
//        ListView listView = mainActivity.findViewById(R.id.listViewProducts);
//
//        listView.setAdapter(adapter);
//    }

    public void ConnectRvAdapterToProducts()
    {
        products = db.GetTableProducts().getAll();
        //products = new ArrayList<>();
        //products.add(new Product(0, "Арбуз", String.valueOf(R.drawable.watermelon), 100));
        //products.add(new Product(1, "Банан", String.valueOf(R.drawable.banan), 50));
        //products.add(new Product(2, "Кокос", String.valueOf(R.drawable.cocos), 70));
        for (int i = 0; i < products.size(); i++)
        {
            Product product = products.get(i);

            int mainPicture = mainActivity.getResources().getIdentifier(product.getPicturePath(),"drawable", mainActivity.getPackageName());

            product.setPictureId(mainPicture);
        }

        rv = mainActivity.findViewById(R.id.rv);

        RvAdapter adapter = new RvAdapter(products);
        rv.setAdapter(adapter);

        GridLayoutManager glm = new GridLayoutManager(mainActivity, 2);
        rv.setLayoutManager(glm);
    }

    public void InitializeButtonClick()
    {
        Button buttonStartSearch = mainActivity.findViewById(R.id.buttonStartSearch);
        buttonStartSearch.setOnClickListener(OnButtonStartSearchClickListener);
    }

    private View.OnClickListener OnButtonStartSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            SearchMethod();
        }
    };

    private void SearchMethod()
    {
        EditText editTextProductName = mainActivity.findViewById(R.id.editTextProductNameSearch);
        String partOfName = editTextProductName.getText().toString();

        ArrayList<Product> foundedProducts = db.GetTableProducts().GetByPartOfName(partOfName);

        Context context = (Context) DataStorage.Get("context");

        RecyclerView recyclerViewSearch = mainActivity.findViewById(R.id.rv);
        GridLayoutManager llm = new GridLayoutManager(context,2);
        recyclerViewSearch.setLayoutManager(llm);

        RvAdapter adapter = new RvAdapter(foundedProducts);
        recyclerViewSearch.setAdapter(adapter);
    }
}

