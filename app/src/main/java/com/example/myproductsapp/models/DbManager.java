package com.example.myproductsapp.models;

import android.content.Context;

import com.example.myproductsapp.models.tables.TableProducts;
import com.example.myproductsapp.models.tools.DbHelper;

public class DbManager
{
    private static DbManager instance = null;

    public static DbManager getInstance(Context context)
    {
        if (instance==null)
        {
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableProducts tableProducts;

    private DbManager(Context context)
    {
        DbHelper dbHelper = new DbHelper(context);

        tableProducts = new TableProducts(dbHelper);
    }

    public TableProducts GetTableProducts() {
        return tableProducts;
    }
}
