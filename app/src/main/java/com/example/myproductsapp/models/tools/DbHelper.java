package com.example.myproductsapp.models.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context context) {
        super(context, "app2.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS \"products\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"picture\"\tTEXT NOT NULL,\n" +
                "\t\"price\"\tINTEGER NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("DELETE FROM products");

        db.execSQL("INSERT INTO \"products\" (\"id\",\"name\",\"picture\",\"price\") VALUES (1,'Арбуз','watermelon',100),\n" +
                " (2,'Банан','banan',50),\n" +
                " (3,'Кокос','cocos',70);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
