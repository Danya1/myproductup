package com.example.myproductsapp.models.entities;

public class Product
{
    private int Id;
    private String Name;
    private String PicturePath;
    private int Price;

    private int PictureId;

    public Product(int id, String name, String picturePath, int price) {
        this.Id = id;
        this.Name = name;
        this.PicturePath = picturePath;
        this.Price = price;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public int getPrice() {
        return Price;
    }

    public String getPicturePath() {
        return PicturePath;
    }

    public int getPictureId() {
        return PictureId;
    }

    public void setPictureId(int pictureId) {
        this.PictureId = pictureId;
    }
}
